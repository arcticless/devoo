package model;


public class Troncon {

  private String label;

  private double vitesse;

  private double distance;

  private Point pointDest;


  public Troncon(String label, double vitesse, double distance, Point pointDest) {
    this.label = label;
    this.vitesse = vitesse;
    this.distance = distance;
    this.pointDest = pointDest;
  }

  public double getDistance() {
    return distance;
  }

  public void setDistance(double distance) {
    this.distance = distance;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Point getPointDest() {
    return pointDest;
  }

  public void setPointDest(Point pointDest) {
    this.pointDest = pointDest;
  }

  public double getVitesse() {
    return vitesse;
  }

  public void setVitesse(double vitesse) {
    this.vitesse = vitesse;
  }
 
  
  public double getTempsTroncon() 
  {
    return (distance/ vitesse);
  }


}