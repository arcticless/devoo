package model;

import java.util.*;

public class Chemin {

    /**
   * 
   * @element-type Troncon
   */
  private List<Troncon>  lesTroncons;
  private Point firstPoint;


  public Chemin( Point firstPoint) {
    this.lesTroncons = new ArrayList<Troncon>();
    this.firstPoint = firstPoint;
  }

  public Point getFirstPoint() {
    return firstPoint;
  }

  public List<Troncon> getLesTroncons() {
    return lesTroncons;
  }

  public void setLesTroncons(List<Troncon> lesTroncons) {
    this.lesTroncons = lesTroncons;
  }
  
  public void Chemin(Point firstPoint, List<Troncon> lesTroncons) 
  {
  }


}