package model;

import controller.Controleur;
import java.util.*;

/*
 */
public class Plan {

    private DemandeLivraisons laDemLivraisons;
    /**
   * 
   * @element-type Point
   */
  private List<Point>  lesPoints;
  private Controleur leControleur;
    /**
   * 
   * @element-type Troncon
   */
  private List<Troncon>  lesTroncons;

  public Plan() {
    this.lesTroncons = new ArrayList<Troncon>();
    this.lesPoints = new ArrayList<Point>();
  }

  public Plan(Controleur leControleur) {
    this.leControleur = leControleur;
    this.lesTroncons = new ArrayList<Troncon>();
    this.lesPoints = new ArrayList<Point>();
  }

  public Plan( Controleur leControleur, List<Point> lesPoints, List<Troncon> lesTroncons) {

    this.leControleur = leControleur;
    this.lesTroncons = new ArrayList<Troncon>(lesTroncons);
    this.lesPoints = new ArrayList<Point>(lesPoints);
  }

  public Controleur getLeControleur() {
    return leControleur;
  }

  public void setLeControleur(Controleur leControleur) {
    this.leControleur = leControleur;
  }

  public DemandeLivraisons getLaDemLivraisons() {
    return laDemLivraisons;
  }

  public void setLaDemLivraisons(DemandeLivraisons laDemLivraisons) {
      this.laDemLivraisons = laDemLivraisons;

  }

  public List<Point> getLesPoints() {
    return lesPoints;
  }

  public void setLesPoints(List<Point> lesPoints) {
    this.lesPoints = lesPoints;
  }

  public List<Troncon> getLesTroncons() {
    return lesTroncons;
  }

  public void setLesTroncons(List<Troncon> lesTroncons) {
    this.lesTroncons = lesTroncons;
  }
  
  public Point getPointPlusProche(int X, int Y) 
  {
    return null;
  }

  public List<Point> getPoints() 
  {
    return getLesPoints();
  }

  public Point getPointById(int id) 
  {
        Point point = null;
        for(Point p : lesPoints){
             if(p.getId() == id){
                 point = p;
                 break;
             }
         }
         return point;
  }

  public void dijkstra() 
  {
  }

}