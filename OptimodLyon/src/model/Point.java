package model;

import java.util.*;

public class Point {

  private int x;

  private int y;

  private int id;

    /**
   * 
   * @element-type Troncon
   */
  private List<Troncon>  lesTronconsSortants;

  public Point(int id, int x, int y ) 
  {
    this.x = x;
    this.y = y;
    this.id = id;
    this.lesTronconsSortants = new ArrayList<Troncon>();
  }

  public int getId() {
    return id;
  }

  public List<Troncon> getLesTronconsSortants() {
    return lesTronconsSortants;
  }

  public void addTroncon(Troncon t)
  {
		this.lesTronconsSortants.add(t);
	}
  
  public void setLesTronconsSortants(List<Troncon> lesTronconsSortants) {
    this.lesTronconsSortants = lesTronconsSortants;
  }

  public int getX() {
    return x;
  }


  public int getY() {
    return y;
  }


  
  public List<Troncon> getTroncons() {
    return getLesTronconsSortants();
  }


}