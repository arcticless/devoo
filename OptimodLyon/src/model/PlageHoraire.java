package model;

import exceptions.PointDejaExistantException;
import java.util.*;

public class PlageHoraire {

    private int heureDebut;

    private int heureFin;

    /**
     *
     * @element-type Livraison
     */
    private List<Livraison> lesLivraisons;

    public PlageHoraire(int heureDebut, int heureFin) {
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
        this.lesLivraisons = new ArrayList<Livraison>();
    }

    public int getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(int heureDebut) {
        this.heureDebut = heureDebut;
    }

    public int getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(int heureFin) {
        this.heureFin = heureFin;
    }

    public List<Livraison> getLesLivraisons() {
        return lesLivraisons;
    }

    public void setLesLivraisons(List<Livraison> lesLivraisons) {
        this.lesLivraisons = lesLivraisons;
    }

    public void ordonnerLivraisons(List<Integer> l) {
    }

    public void addLivraison(Livraison l) {
        this.lesLivraisons.add(l);
    }

    public void addLivraison(int index, Livraison l) {
        this.lesLivraisons.add(index, l);
    }

    /**
     * Récupère une livraison à partir de l'id du point
     *
     * @param idPoint
     * @return Livraison
     */
    public Livraison getLivraisonByPointId(int idPoint) {
        Livraison res = null;
        for (Livraison l : lesLivraisons) {
            if (l.getAdresse().getId() == idPoint) {
                res = l;
                break;
            }
        }
        return res;
    }

    /**
     * Vérifie si l'adresse à livrer est déjà associée à une livraison
     *
     * @param adresseLiv l'id du point à livrer
     * @return true si l'id existe et false sinon
     */
    public boolean adresseLivraisonExiste(int adresseLiv) {
        boolean res = false;
        for (Livraison l : this.lesLivraisons) {
            if (l.getAdresse().getId() == adresseLiv) {
                res = true;
                break;
            }
        }
        return res;
    }

    /**
     * Vérifie si l'id passé en paramètre existe déja dans la liste de
     * livraisons de la plage horaire
     *
     * @param id l'id de la livraison
     * @return true si l'di existe et false sinon
     */
    public boolean idLivraisonExiste(int id) {
        boolean res = false;
        for (Livraison l : this.lesLivraisons) {
            if (l.getIdLivriason() == id) {
                res = true;
                break;
            }
        }
        return res;
    }
}
