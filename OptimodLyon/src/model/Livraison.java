package model;

public class Livraison {

    
  private int idLivriason;
  private PlageHoraire laPlageHoraire;
  private Point adresse;

    public Livraison(int idLivriason, PlageHoraire laPlageHoraire, Point lePoint) {
        this.idLivriason = idLivriason;
        this.laPlageHoraire = laPlageHoraire;
        this.adresse = lePoint;
    }

    public int getIdLivriason() {
        return idLivriason;
    }


  public PlageHoraire getLaPlageHoraire() {
    return laPlageHoraire;
  }

  public void setLaPlageHoraire(PlageHoraire laPlageHoraire) {
    this.laPlageHoraire = laPlageHoraire;
  }

  public Point getAdresse() {
    return adresse;
  }

  public void setLePoint(Point adresse) {
    this.adresse = adresse;
  }
  
 

}