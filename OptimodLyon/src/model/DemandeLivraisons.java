package model;


import java.util.*;

public class DemandeLivraisons {

  private Date date;

    private Plan lePlan;
    /**
   * 
   * @element-type PlageHoraire
   */
   private List<PlageHoraire>  lesPlagesHoraires;
    /**
   * 
   * @element-type Chemin
   */
  private List<Chemin>  lesChemins;
  private GraphePlusCourtChemin graphePCC;
  private Point entrepot;

  public DemandeLivraisons(Date date, Plan lePlan, GraphePlusCourtChemin graphePCC, Point entrepot) {
    this.date = date;
    this.lePlan = lePlan;
    this.graphePCC = graphePCC;
    this.entrepot = entrepot;
    this.lesChemins = new ArrayList<Chemin>();
    this.lesPlagesHoraires = new ArrayList<PlageHoraire>();
  }

  public DemandeLivraisons() {
    this.lesChemins = new ArrayList<Chemin>();
    this.lesPlagesHoraires = new ArrayList<PlageHoraire>();
  }

    public DemandeLivraisons(Plan lePlan, List<PlageHoraire> lesPlagesHoraires, Point entrepot) {
        this.lePlan = lePlan;
        this.entrepot = entrepot;
        this.lesChemins = new ArrayList<Chemin>();
        this.lesPlagesHoraires = new ArrayList<PlageHoraire>();
        for(PlageHoraire plage : lesPlagesHoraires){
            this.lesPlagesHoraires.add(plage);
        }
    }

    public Point getEntrepot() {
        return entrepot;
    }
    
    
    /**
     * Renvoie une liste des plages horaires associé à un point dans la même journée.
     * @param id  l'id du point
     * @return liste des PlageHoraires
     */
    public List<PlageHoraire> getPlagesByPointId(int id){
        List<PlageHoraire> plages = new ArrayList<PlageHoraire>();
        for(PlageHoraire plage : this.lesPlagesHoraires){
            for(Livraison l : plage.getLesLivraisons()){
                if(l.getAdresse().getId() == id){
                    plages.add(plage);
                }
            }
        }
        return plages;
    }
    
    
    
   
    
  public void setPlan( Plan p) 
  {
  }

  public void creerPlageHoraire() 
  {
  }

  public void DemandeLibraisons(Date d) 
  {
  }

  public List<Point> getPointList() 
  {
    return null;
  }

  public List<Integer> ordonnerLivraisons(List<List<Double>> tmp) 
  {
    return null;
  }

  public List<List<Livraison>> getLivraisonsParPlageHoraire() 
  {
    return null;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public GraphePlusCourtChemin getGraphePCC() {
    return graphePCC;
  }

  public void setGraphePCC(GraphePlusCourtChemin graphePCC) {
    this.graphePCC = graphePCC;
  }

  public Plan getLePlan() {
    return lePlan;
  }

  public void setLePlan(Plan lePlan) {
    this.lePlan = lePlan;
  }

  public List<Chemin> getLesChemins() {
    return lesChemins;
  }

  public void setLesChemins(List<Chemin> lesChemins) {
    this.lesChemins = lesChemins;
  }

  public List<PlageHoraire> getLesPlagesHoraires() {
    return lesPlagesHoraires;
  }

  public void setLesPlagesHoraires(List<PlageHoraire> lesPlagesHoraires) {
    this.lesPlagesHoraires = lesPlagesHoraires;
  }

}