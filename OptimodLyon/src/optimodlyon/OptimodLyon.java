/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package optimodlyon;

import controller.*;

import view.Fenetre;

/**
 *
 * @author Anthony QUILFEN
 */
public class OptimodLyon {

  public static void main(String[] args) {

    //Mode Affichage Windows
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Windows".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(Fenetre.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(Fenetre.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(Fenetre.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(Fenetre.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }

    Controleur controleur = new Controleur();
    Utils utilitaire = new Utils(controleur); //Ajout bidirectionnel
    controleur.setUtilitaire(utilitaire);
    Invoker invoker = new Invoker(controleur);
    Fenetre laFenetre = new Fenetre();

    laFenetre.setControleur(controleur);

    laFenetre.setTitle("OptimodLyon - Application - H4402");

    laFenetre.setVisible(true);
  }
}
