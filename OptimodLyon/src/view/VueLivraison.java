package view;

import model.*;

public class VueLivraison {

  private Livraison livraison;
  private VuePlageHoraire vPlageHoraire;
  private VuePoint vPoint;

  public VueLivraison() {
  }

  public VueLivraison(Livraison livraison, VuePlageHoraire vPlageHoraire, VuePoint vPoint) {
    this.livraison = livraison;
    this.vPlageHoraire = vPlageHoraire;
    this.vPoint = vPoint;
  }

  public void dessiner() 
  {
  }

  public void VueLivraison(Livraison l) 
  {
  }

  public Livraison getLivraison() {
    return livraison;
  }

  public void setLivraison(Livraison livraison) {
    this.livraison = livraison;
  }

  public VuePlageHoraire getvPlageHoraire() {
    return vPlageHoraire;
  }

  public void setvPlageHoraire(VuePlageHoraire vPlageHoraire) {
    this.vPlageHoraire = vPlageHoraire;
  }

  public VuePoint getvPoint() {
    return vPoint;
  }

  public void setvPoint(VuePoint vPoint) {
    this.vPoint = vPoint;
  }

}