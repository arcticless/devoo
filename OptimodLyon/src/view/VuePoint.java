package view;

import model.*;
import java.awt.Color;

public class VuePoint {

  private int epaisseur;
  private Color couleur;
  private Point point;
  private VuePlan vPlan;
  private VueLivraison vLivraison;
  private int pixX;
  private int pixY;

  public VuePoint(int epaisseur, Color couleur, Point point, VuePlan vPlan, VueLivraison vLivraison) {
    this.epaisseur = epaisseur;
    this.couleur = couleur;
    this.point = point;
    this.vPlan = vPlan;
    this.vLivraison = vLivraison;
  }

  public VuePoint() {
  }

  public Color getCouleur() {
    return couleur;
  }

  public int getEpaisseur() {
    return epaisseur;
  }

  public void setCouleur(Color couleur) {
    this.couleur = couleur;
  }

  public void setEpaisseur(int epaisseur) {
    this.epaisseur = epaisseur;
  }

  public Point getPoint() {
    return point;
  }

  public void setPoint(Point point) {
    this.point = point;
  }

  public VueLivraison getvLivraison() {
    return vLivraison;
  }

  public void setvLivraison(VueLivraison vLivraison) {
    this.vLivraison = vLivraison;
  }

  public VuePlan getvPlan() {
    return vPlan;
  }

  public void setvPlan(VuePlan vPlan) {
    this.vPlan = vPlan;
  }

  public void dessiner() {
  }

  public void initEtat() {
  }

  public boolean isVuePoint(Point p) {
    return false;
  }

  public int getPixX() {
    return pixX;
  }

  public void setPixX(int pixX) {
    this.pixX = pixX;
  }

  public int getPixY() {
    return pixY;
  }

  public void setPixY(int pixY) {
    this.pixY = pixY;
  }
}