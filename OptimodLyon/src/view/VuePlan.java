package view;

import java.awt.Color;
import model.Plan;
import model.Point;
import java.util.*;
import model.Troncon;

public class VuePlan {

  /**
   * 
   * @element-type VuePoint
   */
  private List<VuePoint> vLesPoints;
  private Plan plan;
  VuePoint pMax;
  VuePoint pMin;
  /**
   * 
   * @element-type VueTroncon
   */
  private List<VueTroncon> vTroncons;

  public VuePlan() {
    this.vLesPoints = new ArrayList<VuePoint>();
    this.vTroncons = new ArrayList<VueTroncon>();
  }

  public VuePlan(Plan plan, int panelWidth, int panelHeight) {
    this.plan = plan;
    this.vLesPoints = new ArrayList<VuePoint>();
    this.vTroncons = new ArrayList<VueTroncon>();
    initAffichage(panelWidth, panelHeight);
  }

  public Plan getPlan() {
    return plan;
  }

  public void setPlan(Plan plan) {
    this.plan = plan;
  }

  public VuePoint getpMax() {
    return pMax;
  }

  public void setpMax(VuePoint pMax) {
    this.pMax = pMax;
  }

  public VuePoint getpMin() {
    return pMin;
  }

  public void setpMin(VuePoint pMin) {
    this.pMin = pMin;
  }

  public List<VuePoint> getvLesPoints() {
    return vLesPoints;
  }

  public void setvLesPoints(List<VuePoint> vLesPoints) {
    this.vLesPoints = vLesPoints;
  }

  public List<VueTroncon> getvTroncons() {
    return vTroncons;
  }

  public void setvTroncons(List<VueTroncon> vTroncons) {
    this.vTroncons = vTroncons;
  }

  private void initAffichage(int panelWidth, int panelHeight) {
    List<Point> pts = this.plan.getPoints();

    List<Troncon> troncons = this.plan.getLesTroncons();

    //Get les points max et min pour affichage correct sur la vue du plan
    this.pMax = new VuePoint();
    this.pMin = new VuePoint();

    pMax.setPoint(pts.get(0));
    pMin.setPoint(pts.get(0));
    int xMax = pMax.getPoint().getX();
    int yMax = pMax.getPoint().getY();
    int xMin = pMin.getPoint().getX();
    int yMin = pMin.getPoint().getY();

    for (int i = 1; i < pts.size(); ++i) {

      Point lePoint = pts.get(i);

      if (lePoint.getX() > xMax) {
        xMax = lePoint.getX();
      }
      if (lePoint.getY() > yMax) {
        yMax = lePoint.getY();
      }
      if (lePoint.getX() < xMin) {
        xMin = lePoint.getX();
      }
      if (lePoint.getY() < yMin) {
        yMin = lePoint.getY();
      }

    }

    //Calcul des coordonnées en pixels de chaque Point pour la VuePoint associée

    for (int i = 0; i < pts.size(); ++i) {
      Point lePoint = pts.get(i);

      VuePoint saVuePoint = new VuePoint(5, Color.black, lePoint, this, null);
      int lePixX = (int) ((lePoint.getX() - xMin) * (panelWidth / ((double) (xMax - xMin))));
      int lePixY = (int) ((lePoint.getY() - yMin) * ((double) panelHeight / ((double) (yMax - yMin))));
      saVuePoint.setPixX(lePixX);
      saVuePoint.setPixY(lePixY);
      this.vLesPoints.add(saVuePoint);

    }

    //Creation des VueTronçon, associés à chaque VuePoint
    //List<Troncon> trcs = this.plan.getLesTroncons();
    for (int i = 0; i < pts.size(); ++i) {
      Point lePoint = pts.get(i);
      List<Troncon> sesTronconsSortants = lePoint.getTroncons();

      for (int j = 0; j < sesTronconsSortants.size(); ++j) {
        Troncon leTroncon = sesTronconsSortants.get(j);
        VueTroncon saVueTroncon = new VueTroncon(1, Color.LIGHT_GRAY, leTroncon, this);

        saVueTroncon.setvPtDepart(getVuePointByReference(lePoint));
        //Récuperer la VuePoint associée au Point de destination du Troncon 
        saVueTroncon.setvPtArrivee(getVuePointByReference(leTroncon.getPointDest()));

        this.vTroncons.add(saVueTroncon);

      }
    }

  }

  public VuePoint getVuePointByReference(Point pt) {
    VuePoint res = null;
    for (int i = 0; i < this.vLesPoints.size(); ++i) {
      if (vLesPoints.get(i).getPoint().getId() == pt.getId()) {
        res = this.vLesPoints.get(i);
        break;
      }
    }

    return res;
  }
}