package view;

import java.util.*;
import model.Chemin;

public class VueChemin {

  private VueDemandeLivraisons vDemLivraisons;
    /**
   * 
   * @element-type VuePoint
   */
  private List<VuePoint>  vPoints;
    /**
   * 
   * @element-type VueTroncon
   */
  private List<VueTroncon>  vTroncons;
  private Chemin chemin;

  public VueChemin() {
    this.vTroncons = new ArrayList<VueTroncon>();
    this.vPoints = new ArrayList<VuePoint>();
  }

  public VueChemin(VueDemandeLivraisons vDemLivraisons, Chemin chemin) {
    this.vDemLivraisons = vDemLivraisons;
    this.chemin = chemin;
    this.vTroncons = new ArrayList<VueTroncon>();
    this.vPoints = new ArrayList<VuePoint>();
  }

  public Chemin getChemin() {
    return chemin;
  }

  public void setChemin(Chemin chemin) {
    this.chemin = chemin;
  }

  public VueDemandeLivraisons getvDemLivraisons() {
    return vDemLivraisons;
  }

  public void setvDemLivraisons(VueDemandeLivraisons vDemLivraisons) {
    this.vDemLivraisons = vDemLivraisons;
  }

  public List<VuePoint> getvPoints() {
    return vPoints;
  }

  public void setvPoints(List<VuePoint> vPoints) {
    this.vPoints = vPoints;
  }

  public List<VueTroncon> getvTroncons() {
    return vTroncons;
  }

  public void setvTroncons(List<VueTroncon> vTroncons) {
    this.vTroncons = vTroncons;
  }

  public void dessiner() 
  {
  }

  public List<Chemin> getChemins() 
  {
    return null;
  }

  public void VueChemin(VuePoint vFirstPoint, List<VueTroncon> t) 
  {
  }

}