package view;

import model.*;
import java.util.*;

public class VuePlageHoraire {

    /**
   * 
   * @element-type VueLivraison
   */
  private List<VueLivraison>  vLivraisons;
  private PlageHoraire plageHoraire;

  public VuePlageHoraire() {
    this.vLivraisons = new ArrayList<VueLivraison>();
  }

  public VuePlageHoraire(PlageHoraire plageHoraire) {
    this.plageHoraire = plageHoraire;
    this.vLivraisons = new ArrayList<VueLivraison>();
  }

  public void dessiner() 
  {
  }

  public void creerVueLivraison() 
  {
  }

  public PlageHoraire getPlageHoraire() {
    return plageHoraire;
  }

  public void setPlageHoraire(PlageHoraire plageHoraire) {
    this.plageHoraire = plageHoraire;
  }

  public List<VueLivraison> getvLivraisons() {
    return vLivraisons;
  }

  public void setvLivraisons(List<VueLivraison> vLivraisons) {
    this.vLivraisons = vLivraisons;
  }

}