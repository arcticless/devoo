package view;

import model.Troncon;
import java.awt.Color;

public class VueTroncon {

  private int epaisseur;
  private Color couleur;
  private Troncon troncon;
  private VuePlan vPlan;
  private VuePoint vPtDepart;
  private VuePoint vPtArrivee;

  public VueTroncon(int epaisseur, Color couleur, Troncon troncon, VuePlan vPlan) {
    this.epaisseur = epaisseur;
    this.couleur = couleur;
    this.troncon = troncon;
    this.vPlan = vPlan;
  }

  public VueTroncon() {
  }

  public int getEpaisseur() {
    return epaisseur;
  }

  public void setEpaisseur(int epaisseur) {
    this.epaisseur = epaisseur;
  }

  public Troncon getTroncon() {
    return troncon;
  }

  public void setTroncon(Troncon troncon) {
    this.troncon = troncon;
  }

  public VuePlan getvPlan() {
    return vPlan;
  }

  public void setvPlan(VuePlan vPlan) {
    this.vPlan = vPlan;
  }

  public void dessiner() {
  }

  public void initEtat() {
  }

  public void setCouleur(Color c) {
  }

  public Color getCouleur() {
    return couleur;
  }

  public boolean IsVueTroncon(Troncon t) {
    return false;
  }

  public VuePoint getvPtArrivee() {
    return vPtArrivee;
  }

  public void setvPtArrivee(VuePoint vPtArrivee) {
    this.vPtArrivee = vPtArrivee;
  }

  public VuePoint getvPtDepart() {
    return vPtDepart;
  }

  public void setvPtDepart(VuePoint vPtDepart) {
    this.vPtDepart = vPtDepart;
  }
}