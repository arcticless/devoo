package view;

import model.*;
import java.util.*;

public class VueDemandeLivraisons {

  private DemandeLivraisons demandeLivraisons;
      /**
   * 
   * @element-type VueChemin
   */
  private List<VueChemin>  vLesChemins;
    /**
   * 
   * @element-type VuePlageHoraire
   */
  private List<VuePlageHoraire>  vPlagesHoraires;

  public VueDemandeLivraisons() {
    this.vLesChemins = new ArrayList<VueChemin>();
    this.vPlagesHoraires = new ArrayList<VuePlageHoraire>();
  }

  public DemandeLivraisons getDemandeLivraisons() {
    return demandeLivraisons;
  }

  public void setDemandeLivraisons(DemandeLivraisons demandeLivraisons) {
    this.demandeLivraisons = demandeLivraisons;
  }

  public List<VueChemin> getvLesChemins() {
    return vLesChemins;
  }

  public void setvLesChemins(List<VueChemin> vLesChemins) {
    this.vLesChemins = vLesChemins;
  }

  public List<VuePlageHoraire> getvPlagesHoraires() {
    return vPlagesHoraires;
  }

  public void setvPlagesHoraires(List<VuePlageHoraire> vPlagesHoraires) {
    this.vPlagesHoraires = vPlagesHoraires;
  }

  public void dessiner() 
  {
  }

  public void getVueLivraison(int id) 
  {
  }

  public List<Chemin> getChemins() 
  {
    return null;
  }

  public void setVueChemin(List<VueChemin> vChemins) 
  {
  }

  public void creerVueLivraison() 
  {
  }

}