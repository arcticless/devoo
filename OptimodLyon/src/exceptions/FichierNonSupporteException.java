/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Spownik
 */
public class FichierNonSupporteException extends Exception {

  public FichierNonSupporteException() {
  }

  public FichierNonSupporteException(String message) {
    super(message);
  }
  
  
}
