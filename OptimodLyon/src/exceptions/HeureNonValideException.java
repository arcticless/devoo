/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author k.okba
 */
public class HeureNonValideException extends Exception {

    /**
     * Creates a new instance of
     * <code>HeureNonValideException</code> without detail message.
     */
    public HeureNonValideException() {
    }

    /**
     * Constructs an instance of
     * <code>HeureNonValideException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public HeureNonValideException(String msg) {
        super(msg);
    }
}
