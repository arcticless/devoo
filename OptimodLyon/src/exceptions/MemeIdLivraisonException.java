/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author k.okba
 */
public class MemeIdLivraisonException extends Exception {

    /**
     * Creates a new instance of
     * <code>MemeIdLivraisonException</code> without detail message.
     */
    public MemeIdLivraisonException() {
    }

    /**
     * Constructs an instance of
     * <code>MemeIdLivraisonException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public MemeIdLivraisonException(String msg) {
        super(msg);
    }
}
