/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author k.okba
 */
public class EntrepotNonRenseigneException extends Exception {

    /**
     * Creates a new instance of
     * <code>EntrepotNonRenseigneException</code> without detail message.
     */
    public EntrepotNonRenseigneException() {
    }

    /**
     * Constructs an instance of
     * <code>EntrepotNonRenseigneException</code> with the specified detail
     * message.
     *
     * @param msg the detail message.
     */
    public EntrepotNonRenseigneException(String msg) {
        super(msg);
    }
}
