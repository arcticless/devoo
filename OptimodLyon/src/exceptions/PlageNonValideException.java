/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author k.okba
 */
public class PlageNonValideException extends Exception {

    /**
     * Creates a new instance of
     * <code>PlageNonValideException</code> without detail message.
     */
    public PlageNonValideException() {
    }

    /**
     * Constructs an instance of
     * <code>PlageNonValideException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public PlageNonValideException(String msg) {
        super(msg);
    }
}
