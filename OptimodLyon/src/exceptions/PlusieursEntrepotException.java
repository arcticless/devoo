/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author k.okba
 */
public class PlusieursEntrepotException extends Exception {

    /**
     * Creates a new instance of
     * <code>PlusieursEntrepotException</code> without detail message.
     */
    public PlusieursEntrepotException() {
    }

    /**
     * Constructs an instance of
     * <code>PlusieursEntrepotException</code> with the specified detail
     * message.
     *
     * @param msg the detail message.
     */
    public PlusieursEntrepotException(String msg) {
        super(msg);
    }
}
