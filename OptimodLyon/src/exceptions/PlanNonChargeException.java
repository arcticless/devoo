/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author k.okba
 */
public class PlanNonChargeException extends Exception {

    /**
     * Creates a new instance of
     * <code>PlanNonChargeException</code> without detail message.
     */
    public PlanNonChargeException() {
    }

    /**
     * Constructs an instance of
     * <code>PlanNonChargeException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public PlanNonChargeException(String msg) {
        super(msg);
    }
}
