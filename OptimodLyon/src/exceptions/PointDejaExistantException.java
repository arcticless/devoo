package exceptions;

public class PointDejaExistantException extends Exception{

    public PointDejaExistantException() {
    }

    public PointDejaExistantException(String message) {
        super(message);
    }
	
}

