import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;
import org.xml.sax.SAXException;


public class Utils {
	
	public static void chargerPlan(String nomFichier){
		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance();   
		// cr�ation d'un constructeur de documents 
		try{
			DocumentBuilder constructeur = fabrique.newDocumentBuilder();
			File fichierXml = new File(nomFichier);
			Document document = constructeur.parse(fichierXml);
			DocumentTraversal traversal = (DocumentTraversal) document;
			
			NodeIterator iterator = traversal.createNodeIterator(document.getDocumentElement(), NodeFilter.SHOW_ELEMENT, null, true);
			for (Node n = iterator.nextNode(); n != null; n = iterator.nextNode()) {
				Element e = (Element) n;
				if(e.getTagName() == "Noeud"){
					System.out.println(e.getAttribute("id"));
				}
			}
			
		}catch(ParserConfigurationException pce){ 
			System.out.println("Erreur de configuration du parseur DOM"); 
			System.out.println("lors de l'appel � fabrique.newDocumentBuilder();"); 
		}catch(SAXException se){ 
			System.out.println("Erreur lors du parsing du document"); 
			System.out.println("lors de l'appel � construteur.parse(xml)"); 
		}catch(IOException ioe){ 
			System.out.println("Erreur d'entr�e/sortie"); 
			System.out.println("lors de l'appel � construteur.parse(xml)"); 
		}
		
		
		
		
	}
	
	
	
	
	
	
}
