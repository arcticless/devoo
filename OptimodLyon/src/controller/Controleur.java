package controller;


import exceptions.EntrepotNonRenseigneException;
import exceptions.HeureNonValideException;
import exceptions.MemeIdLivraisonException;
import exceptions.PlageNonValideException;
import exceptions.PlanNonChargeException;
import exceptions.PlusieursEntrepotException;
import exceptions.FichierNonSupporteException;
import exceptions.LabelNonValideException;
import exceptions.PointDejaExistantException;
import exceptions.PointNonExistantException;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import model.DemandeLivraisons;
import model.Plan;
import model.Point;
import org.xml.sax.SAXException;
import view.Fenetre;


/*
 */
public class Controleur {

  private Plan lePlan;
  private Invoker invoqueur;
  private Utils utilitaire;
  private Fenetre laFenetre;

  public Controleur()
  {
    
  }

  public void ouvrirPlan(String filename) throws PointNonExistantException, ParserConfigurationException, PointDejaExistantException, IOException, SAXException, LabelNonValideException, NumberFormatException, FichierNonSupporteException 
  {
    lePlan = utilitaire.chargerPlan(filename);
  }

  public void ouvrirDemLivraisons(String filename) throws PlanNonChargeException, ParserConfigurationException, SAXException, IOException, PointNonExistantException, NumberFormatException, FichierNonSupporteException, HeureNonValideException, PlageNonValideException, EntrepotNonRenseigneException, PlusieursEntrepotException, MemeIdLivraisonException, PointDejaExistantException 
  {
     
      if(lePlan==null){
          throw new PlanNonChargeException("Erreur : Il n'y a aucun plan chargé pour cette demande de livraisons.");
      }else{
         lePlan.setLaDemLivraisons(utilitaire.chargerDemLivraisons(filename));    
      }
      
  }

  public Plan getLePlan() {
    return lePlan;
  }

  public void calculTournee() 
  {
  }

  public Point getPointById(int id) 
  {

		return null;
			
  }
  
  public void setLePlan(Plan plan){
      this.lePlan = plan;
  }

  public void setUtilitaire(Utils utilitaire) {
    this.utilitaire = utilitaire;
  }

  public void creerVueTournee() 
  {
  }

}