package controller;

import exceptions.EntrepotNonRenseigneException;
import exceptions.HeureNonValideException;
import exceptions.MemeIdLivraisonException;
import exceptions.PlageNonValideException;
import exceptions.PlusieursEntrepotException;
import exceptions.*;
import java.io.IOException;
import model.*;
import java.util.ArrayList;
import java.util.List;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.*;
import org.w3c.dom.traversal.*;
import org.xml.sax.SAXException;

public class Utils {

    private Controleur controleur;

    public Utils(Controleur controleur) {
        this.controleur = controleur;
    }

    public Controleur getControleur() {
        return controleur;
    }

    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }

    public Plan chargerPlan(String filename) throws PointNonExistantException, NumberFormatException, LabelNonValideException, PointDejaExistantException, IOException, SAXException, ParserConfigurationException, FichierNonSupporteException {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder loader = factory.newDocumentBuilder();
            Document document = loader.parse(filename);
            DocumentTraversal traversal = (DocumentTraversal) document;

            Element racine = document.getDocumentElement();

            if (!racine.getNodeName().equals("Reseau")) {
                throw new FichierNonSupporteException("Fichier XML non supporté par l'application.");
            }

            try {
                List<Point> points = creerPoints(document, traversal);
                try {
                    List<Troncon> troncons = ajouterTranconsAuxPoints(document, traversal, points);
                    Plan plan = new Plan(controleur, points, troncons);

                    return plan;
                } catch (PointNonExistantException ex) {
                    throw ex;
                } catch (LabelNonValideException ex) {
                    throw ex;
                }

            } catch (PointDejaExistantException ex) {
                throw ex;
            } catch (NumberFormatException ex) {
                throw ex;
            }

        } catch (ParserConfigurationException pce) {
            throw pce;
        } catch (SAXException se) {
            throw se;
        } catch (IOException ioe) {
            throw ioe;
        }

    }

    public DemandeLivraisons chargerDemandeLivraisons(String filename) {
        return null;
    }

    private static Point getPointById(int id, List<Point> points) {
        Point res = null;
        for (Point p : points) {
            if (p.getId() == id) {
                res = p;
                break;
            }
        }
        return res;

    }

    private static List<Point> creerPoints(Document document, DocumentTraversal traversal) throws PointDejaExistantException {

        List<Point> points = new ArrayList<Point>();
        NodeIterator iterator = traversal.createNodeIterator(document.getDocumentElement(), NodeFilter.SHOW_ELEMENT, null, true);
        for (Node n = iterator.nextNode(); n != null; n = iterator.nextNode()) {
            Element e = (Element) n;
            if (e.getTagName().equals("Noeud")) {
                try {
                    int id = Integer.parseInt(e.getAttribute("id"));
                    int x = Integer.parseInt(e.getAttribute("x"));
                    int y = Integer.parseInt(e.getAttribute("y"));
                    if (id < 0 || x < 0 || y < 0) {
                        throw new NumberFormatException();
                    }

                    // si le point existe déjà, on lève une exception
                    if (getPointById(id, points) != null) {
                        throw new PointDejaExistantException("Erreur sémantique : L'id " + id + " est le même pour deux noeuds différents.");
                    }
                    Point p = new Point(id, x, y);

                    points.add(p);
                } catch (NumberFormatException ex) {
                    throw new NumberFormatException();
                }


            }
        }
        return points;
    }

    private static List<Troncon> ajouterTranconsAuxPoints(Document document, DocumentTraversal traversal, List<Point> points) throws LabelNonValideException, PointNonExistantException, NumberFormatException, PointDejaExistantException {
        List<Troncon> troncons = new ArrayList<Troncon>();
        NodeIterator iterator = traversal.createNodeIterator(document.getDocumentElement(), NodeFilter.SHOW_ELEMENT, null, true);
        for (Node n = iterator.nextNode(); n != null; n = iterator.nextNode()) {
            if (n.getNodeName().equals("Noeud")) {
                Element e = (Element) n;
               
                NodeList childs = n.getChildNodes();
                for (int i = 1; i < childs.getLength(); i++) {
                    if (childs.item(i).getNodeType() == Node.ELEMENT_NODE) {
                        Element e1 = (Element) childs.item(i);
                        try {
                            int idNoeud = Integer.parseInt(e.getAttribute("id"));
                            // on récupère les informations du troncon
                            String label = e1.getAttribute("nomRue");
                            double v = Double.parseDouble(e1.getAttribute("vitesse").replace(",", "."));
                            double l = Double.parseDouble(e1.getAttribute("longueur").replace(",", "."));
                            if (v <= 0 || l <= 0) {
                                throw new NumberFormatException();
                            }
                            if (label == null || label.equals("")) {
                                throw new LabelNonValideException();
                            }
                            int idNoeudDest = Integer.parseInt(e1.getAttribute("idNoeudDestination"));
                            if(idNoeud == idNoeudDest){
                                throw new PointDejaExistantException("Erreur sémantique : Le point de destination et le point de départ sont les mêmes.");
                            }
                            // on récupère le point destination du troncon
                            Point p = getPointById(idNoeudDest, points);
                            // si le point n'existe pas alors on lève une exception pour signaler une erreur sémantique
                            if (p == null) {
                                throw new PointNonExistantException("Le point d'id" + idNoeudDest +" et lié au tronçon " + label + " n'existe pas." );
                            }
                            // on crée le troncon
                            Troncon t = new Troncon(label, v, l, p);
                            troncons.add(t);

                            getPointById(Integer.parseInt(e.getAttribute("id")), points).addTroncon(t);
                        } catch (NumberFormatException ex) {
                            throw new NumberFormatException("Erreur sémantique : Nombre non valide.");
                        }
                    }
                }
            }
        }
        return troncons;
    }

   	
	/**
         * Crer une demande de livraison à partir d'un fichier xml valide
         * 
         * @param nomFichier le nom du fihcier à charger
         * @param plan un objet plan qui contient les points et les tronçons de la ville
         * @return une demande de livraison si le fichier est syntaxiquement et sémantiquement correct
         * si non on lève une exception
         */
        
        public DemandeLivraisons chargerDemLivraisons(String nomFichier) throws PointNonExistantException,NumberFormatException, HeureNonValideException, PlageNonValideException, EntrepotNonRenseigneException, PlusieursEntrepotException, MemeIdLivraisonException, PointDejaExistantException, ParserConfigurationException, SAXException, IOException, FichierNonSupporteException{
            
            DemandeLivraisons demandeL = null;
            List<PlageHoraire> plagesHoraires = new ArrayList<PlageHoraire>();
            try{
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder loader = factory.newDocumentBuilder();
                Document document = loader.parse(nomFichier);
                Plan lePlan = controleur.getLePlan();
                
                if(!document.getDocumentElement().getTagName().equals("JourneeType")){
                    throw new FichierNonSupporteException("Fichier XML non supporté par l'application.");
                }
                
                // on crée l'entrepot
                Point entrepot = creerEntrepot(document, lePlan);
                // récuperer les plages horaires
                plagesHoraires = creerPlagesHoraires(document, lePlan);
                // on crée la demande des livraisons
                demandeL = new DemandeLivraisons(lePlan, plagesHoraires, entrepot);
                 
            } catch (ParserConfigurationException pce) {
                throw new ParserConfigurationException("Fichier erroné (PARSING)");
            } catch (SAXException se) {
              throw se;

            } catch (IOException ioe) {
                throw new IOException("Fichier introuvable ou interdit en accès");

            }
            return demandeL;
        }
          
        
        /**
         * transfome une chaine de la forme hh:mm:ss qui represente une heure en second
         * @param chaine l'heure à transfomer 
         * @return un entier qui represente l'heure en secondes
         * @throws HeureNonValideException 
         */
        private static int toSecond(String chaine) throws HeureNonValideException{
            String[] str = chaine.split(":");
            int heure, minute, second, heureComplet;
            if(str.length!=3){
                throw new HeureNonValideException("Erreur Sémantique : l'heure " + chaine + " n'est pas valide.");
            }else{
                try{
                    heure = Integer.parseInt(str[0]);
                    minute = Integer.parseInt(str[1]);
                    second = Integer.parseInt(str[2]);
                    
                    if(heure>23 || heure<0 || minute<0 || minute>59 || second>59 || second<0){
                        throw new HeureNonValideException("Erreur Sémantique : l'heure " + chaine + " n'est pas valide.");
                    }else{
                        heureComplet = heure*3600 + minute*60 + second;                      
                    }
                    
                   
                }catch(NumberFormatException nfe){
                    throw new HeureNonValideException("Erreur Sémantique : l'heure " + chaine + " n'est pas valide.");
                }
              
            }
            return heureComplet;
        }
        
         
        
        /**
         * Crée un Point qui répresente l'entrepot à partir d'un fichier xml
         * Contrat : le document doit contenir un seul element entrepot
         * @param document document xml à partir duquel, on va extraire les informations de l'entrepot
         * @param plan Un objet Plan qui represente le plan de la ville où on fera les livraisons
         * @return Point 
         * @throws PointNonExistantException  si l'id de lentrepot n'est pas dans le plan
         * @throws EntrepotNonRenseigneException si l'element "entrepot" n'est pas dans le fichier xml
         * @throws PlusieursEntrepotException si on a plusieurs elements "entrepot" dans le fichier xml
         */
         private static Point creerEntrepot(Document document, Plan plan) throws PointNonExistantException, EntrepotNonRenseigneException, PlusieursEntrepotException{
            
             NodeList entrepots = document.getElementsByTagName("Entrepot");
             if(entrepots.getLength()<1){
                 throw new EntrepotNonRenseigneException("Erreur Sémantique : L'entrepot n'est pas renseigné");
             }
             if(entrepots.getLength()>1){
                 throw new PlusieursEntrepotException("Erreur Sémantique : Il y a plusieurs entrepots rensignés. Veuillez garder qu'un seul.");
             }
             Element e = (Element)entrepots.item(0);
             try{

                  Point entrepot = plan.getPointById(Integer.parseInt(e.getAttribute("adresse")));
                  if(entrepot == null){
                    throw new PointNonExistantException("Erreur sémantique : L'adresse associée à l'entrepot n'existe pas.");
                  }
                  return entrepot;
             }catch(NumberFormatException nfe){
                 throw new NumberFormatException("Erreur sémantique : Nombre non valide");
             }
            
            
        }
        
         private static List<PlageHoraire> creerPlagesHoraires(Document document, Plan plan) throws HeureNonValideException, PlageNonValideException, MemeIdLivraisonException, PointDejaExistantException, PointNonExistantException{
                List<PlageHoraire> plagesHoraires = new ArrayList<PlageHoraire>();
                NodeList plages = document.getElementsByTagName("Plage");
                for(int i = 0; i<plages.getLength(); i++){
                    Element plage = (Element) plages.item(i);
                    String heureD = plage.getAttribute("heureDebut");
                    String heureF = plage.getAttribute("heureFin");
                   
                    int heureDebut = toSecond(heureD);
                    int heureFin = toSecond(heureF);
                    // on vérifié si cette plage horaire se chevauche avec la dernière
                    plageHoraireValide(heureDebut, heureFin, plagesHoraires, i);
                    if(heureDebut>=heureFin){
                        throw new PlageNonValideException("Erreur Sémantique : Le plage horaire : " + heureD + " à " + heureF + " est non valide.");
                    }
                    PlageHoraire pHoraire = new PlageHoraire(heureDebut, heureFin);
                    NodeList livraisons = plage.getElementsByTagName("Livraison");
                    for(int j=0; j<livraisons.getLength(); j++){
                        String id = ((Element)livraisons.item(j)).getAttribute("id");
                        try{
                            int idLiv = Integer.parseInt(id);
                            if(idLiv<1){
                                throw new NumberFormatException("Erreur sémantique : l'id de la livraison " + idLiv + "n'est pas valide");
                            }
                            String adresse =((Element)(livraisons.item(j))).getAttribute("adresse");
                            try{
                                int adresseLiv = Integer.parseInt(adresse);
                              
                                if(pHoraire.idLivraisonExiste(idLiv)){
                                    throw new MemeIdLivraisonException("Erreur Sémantique : L'id "+ idLiv + " existe plusieurs fois dans la plage horaire " + heureD + "-" + heureF);
                                }
                                if(pHoraire.adresseLivraisonExiste(adresseLiv)){
                                    throw new PointDejaExistantException("Erreur Sémantique : L'adresse " + adresseLiv + " existe plusieurs fois dans la plage horaire " + heureD + "-" + heureF );
                                }
                                Point pointLiv = plan.getPointById(adresseLiv);
                                if(pointLiv == null){
                                    throw new PointNonExistantException("Erreur sémantique : L'adresse " + adresseLiv + " associée à la livraison d'id " + idLiv + " de la plage horaire " + heureD+"-"+heureF + " n'existe pas");
                                }
                                
                                Livraison liv = new Livraison(idLiv,pHoraire, pointLiv);
                                pHoraire.addLivraison(liv);
                            }catch(NumberFormatException nfe){
                                 throw new NumberFormatException("Erreur sémantique : L'adresse de la livraison " + adresse + " est non valide");
                            }
                           
                        }catch(NumberFormatException nfe){
                            throw new NumberFormatException("Erreur sémantique : L'id de la livraison " + id + " est non valide");
                        }
                       
                    }
                    plagesHoraires.add(pHoraire);
                }
                 return plagesHoraires;
         }
         
         private static boolean plageHoraireValide(int heureDebut, int heureFin,List<PlageHoraire> plages, int index) throws PlageNonValideException{
            if(index>0){
                PlageHoraire lastPlage = plages.get(plages.size()-1);
                if(heureDebut < lastPlage.getHeureFin()){
                    throw new PlageNonValideException("Erreur sémantique : Il y a deux plages horaires qui se chevauchent");
                }
            }
            return true;
         }
         
        
}