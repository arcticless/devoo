package controller;

import model.Point;

public abstract class CmdAjouterPoint extends Commande {

  protected Point pointAjoute;

  protected Point pointSelect;
 
  protected Invoker invoker;
  
  public CmdAjouterPoint(Point pointAjoute, Point pointSelect, Invoker invok) 
  {
    this.pointAjoute = pointAjoute;
    this.pointSelect = pointSelect;
    this.invoker = invok;
  }
}