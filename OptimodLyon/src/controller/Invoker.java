package controller;

import java.util.*;
import model.Point;


public class Invoker {

    private Controleur controleur;
    /**
   * 
   * @element-type Commande
   */
  private List<Commande>  lesCommandes;

  public Invoker(Controleur controleur) {
    this.controleur = controleur;
    this.lesCommandes = new ArrayList<Commande>();
  }

  public void addThenExecute(List<Object> cmd) 
  {
      String commande = (String) cmd.get(0);
      if(commande.equals("ajouterPointApres")){
          
          Point pointAjoute = (Point) cmd.get(1);
          Point pointSelectionne = (Point) cmd.get(2);
          Commande c = new CmdAjouterPointApres(pointAjoute, pointSelectionne, this);
          c.todo();
          lesCommandes.add(c);
      }
  }

  public Controleur getControleur() {
    return controleur;
  }

  public void setControleur(Controleur controleur) {
    this.controleur = controleur;
  }


}