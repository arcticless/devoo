/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import exceptions.EntrepotNonRenseigneException;
import exceptions.HeureNonValideException;
import exceptions.MemeIdLivraisonException;
import exceptions.PlageNonValideException;
import exceptions.PointDejaExistantException;
import exceptions.PointNonExistantException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import model.DemandeLivraisons;
import model.PlageHoraire;
import model.Plan;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.w3c.dom.Document;
import org.xml.sax.SAXParseException;

/**
 *
 * @author Simon
 */
public class UtilsTest {
    
    private Controleur controleur;
    private Utils utils;
    private final String pathPlan = "testPlan";
    private final String pathddl = "testDDL";
    public UtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws Exception{
        controleur = new Controleur();
        utils = new Utils(controleur);  
        controleur.setLePlan(utils.chargerPlan("test.xml"));
    }
    
    @After
    public void tearDown() {
    }

    /**
     * test tous les cas de chargement d'un plan
     */
    @Test
    public void testChargerPlan1() throws Exception {
        System.out.println("ChargerPlan(): fichier juste");
        utils.chargerPlan(pathPlan+"/test1.xml");
    }
   
    /**
     * test tous les cas de chargement d'un plan
     */ 
    @Test(expected = PointNonExistantException.class)
    public void testChargerPlan2() throws Exception {
        System.out.println("ChargerPlan(): noeud inexistants");
        utils.chargerPlan(pathPlan+"/test2.xml");
    }
    
    /**
     * test tous les cas de chargement d'un plan
     */
    @Test(expected = NumberFormatException.class)
    public void testChargerPlan3() throws Exception {
        System.out.println("ChargerPlan(): x<0");
        utils.chargerPlan(pathPlan+"/test3.xml");
    }
    
        
    /**
     * test tous les cas de chargement d'un plan
     */
    @Test(expected = NumberFormatException.class)
    public void testChargerPlan4() throws Exception {
        System.out.println("ChargerPlan(): vitesse=0");
        utils.chargerPlan(pathPlan+"/test4.xml");
    }

    /**
     * test tous les cas de chargement d'un plan
     */
    @Test(expected = NumberFormatException.class)
    public void testChargerPlan5() throws Exception {
        System.out.println("ChargerPlan(): longueur<0");
        utils.chargerPlan(pathPlan+"/test5.xml");
    }
    
    /**
     * test tous les cas de chargement d'un plan
     */
    @Test(expected = NumberFormatException.class)
    public void testChargerPlan6() throws Exception {
        System.out.println("ChargerPlan(): id<0");
        utils.chargerPlan(pathPlan+"/test6.xml");
    }
    
    
    /**
     * test tous les cas de chargement d'un plan
     */
    @Test(expected = NumberFormatException.class)
    public void testChargerPlan7() throws Exception {
        System.out.println("ChargerPlan(): y<0");
        utils.chargerPlan(pathPlan+"/test7.xml");
    }
        
    /**
     * test tous les cas de chargement d'un plan
     */
    @Test(expected = SAXParseException.class)
    public void testChargerPlan8() throws Exception {
        System.out.println("ChargerPlan(): syntaxe incorrecte");
        utils.chargerPlan(pathPlan+"/test8.xml");
    }
    
    /**
     * test tous les cas de chargement d'un plan
     */
    @Test(expected = PointDejaExistantException.class)
    public void testChargerPlan9() throws Exception {
        System.out.println("ChargerPlan(): troncon de A vers A");
        utils.chargerPlan(pathPlan+"/test9.xml");
    }
    
       /**
     * test tous les cas de chargement d'un plan
     */
    @Test(expected = PointDejaExistantException.class)
    public void testChargerPlan10() throws Exception {
        System.out.println("ChargerPlan(): deux id identiques");
        utils.chargerPlan(pathPlan+"/test10.xml");
    }
    
    /**
     * test tous les cas de chargement d'un plan
     */
    @Test
    public void testChargerPlan11() throws Exception {
        System.out.println("ChargerPlan(): noeuds désordonnées");
        utils.chargerPlan(pathPlan+"/test11.xml");
    }
        

    /**
     * Test tous les cas de chargemet d'une demande de livraison.
     */
    @Test
    public void testChargerDemLivraisons1() throws Exception {
       System.out.println("chargerDemLivraisons() : syntaxe correcte");
       utils.chargerDemLivraisons(pathddl+"/test1.xml");
    }
    
     /**
     * Test tous les cas de chargemet d'une demande de livraison.
     */
    @Test(expected = EntrepotNonRenseigneException.class)
    public void testChargerDemLivraisons2() throws Exception {
       System.out.println("chargerDemLivraisons() : entrepot non renseigné");
       utils.chargerDemLivraisons(pathddl+"/test2.xml");
    }
    
    /**
     * Test tous les cas de chargemet d'une demande de livraison.
     */
    @Test(expected = PlageNonValideException.class)
    public void testChargerDemLivraisons3() throws Exception {
       System.out.println("chargerDemLivraisons() : plage horaire incorrecte");
       utils.chargerDemLivraisons(pathddl+"/test3.xml");
    }
    
    /**
     * Test tous les cas de chargemet d'une demande de livraison.
     */
    @Test(expected = NumberFormatException.class)
    public void testChargerDemLivraisons4() throws Exception {
       System.out.println("chargerDemLivraisons() : id non valide");
       utils.chargerDemLivraisons(pathddl+"/test4.xml");
    }
    
    
    /**
     * Test tous les cas de chargemet d'une demande de livraison.
     */
    @Test(expected = PlageNonValideException.class)
    public void testChargerDemLivraisons5() throws Exception {
       System.out.println("chargerDemLivraisons() : plage horaire se chevauchant");
       utils.chargerDemLivraisons(pathddl+"/test5.xml");
    }
    
    /**
     * Test tous les cas de chargemet d'une demande de livraison.
     */
    @Test(expected = NumberFormatException.class)
    public void testChargerDemLivraisons6() throws Exception {
       System.out.println("chargerDemLivraisons() : numéro client négatif");
       utils.chargerDemLivraisons(pathddl+"/test6.xml");
    }
    
    /**
     * Test tous les cas de chargemet d'une demande de livraison.
     */
    @Test(expected = MemeIdLivraisonException.class)
    public void testChargerDemLivraisons7() throws Exception {
       System.out.println("chargerDemLivraisons() : id livraison identiques");
       utils.chargerDemLivraisons(pathddl+"/test7.xml");
    }
    
    /**
     * Test tous les cas de chargemet d'une demande de livraison.
     */
    @Test(expected = PointNonExistantException.class)
    public void testChargerDemLivraisons8() throws Exception {
       System.out.println("chargerDemLivraisons() : addresse négative ");
       utils.chargerDemLivraisons(pathddl+"/test8.xml");
    }
    
     /**
     * Test tous les cas de chargemet d'une demande de livraison.
     */
    @Test(expected = PlageNonValideException.class)
    public void testChargerDemLivraisons9() throws Exception {
       System.out.println("chargerDemLivraisons() : plage horaire vide ");
       utils.chargerDemLivraisons(pathddl+"/test9.xml");
    }
    
    /**
     * Test tous les cas de chargemet d'une demande de livraison.
     */
    @Test(expected = SAXParseException.class)
    public void testChargerDemLivraisons10() throws Exception {
       System.out.println("chargerDemLivraisons() : syntaxe incorrecte ");
       utils.chargerDemLivraisons(pathddl+"/test10.xml");
    }

    
   
    
}
