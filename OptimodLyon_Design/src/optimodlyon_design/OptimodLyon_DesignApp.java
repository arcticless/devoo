/*
 * OptimodLyon_DesignApp.java
 */

package optimodlyon_design;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The main class of the application.
 */
public class OptimodLyon_DesignApp extends SingleFrameApplication {

    /**
     * At startup create and show the main frame of the application.
     */
    @Override protected void startup() {
        show(new OptimodLyon_DesignView(this));
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override protected void configureWindow(java.awt.Window root) {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of OptimodLyon_DesignApp
     */
    public static OptimodLyon_DesignApp getApplication() {
        return Application.getInstance(OptimodLyon_DesignApp.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args) {
        launch(OptimodLyon_DesignApp.class, args);
    }
}
